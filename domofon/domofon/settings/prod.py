from ..settings import *

from ..settings import *  # noqa
from envparse import env

env.read_envfile()

DEBUG = False

ALLOWED_HOSTS = ['sip-domofon.ru']  # it's free, hurry up!

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PASS'),
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

client_db = {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env('CLIENT_DB_NAME'),
        'USER': env('CLIENT_DB_USER'),
        'PASSWORD': env('CLIENT_DB_PASS'),
        'HOST': env('CLIENT_DB_HOST'),
        'PORT': '3306'
    }

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'filter_custom': {
            '()': 'core.log.UserFilter'
        }
    },
    'handlers': {
        'file': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, '..', 'logs/warnings.log'),
        },
    },
    'loggers': {
        'django': {
            'handlers': ['save_to_db', 'file'],
            'level': 'INFO',
            'propagate': True,
        },
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'yestimate.calc@gmail.com'
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
DEFAULT_FROM_EMAIL = 'Yestimate <yestimate.calc@gmail.com>'

ADMIN_EMAIL = 'admin@yestimate.ru'