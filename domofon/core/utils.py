import json
import os
import random
import string
from contextlib import closing

import phonenumbers
import pymysql
from core.custom_exceptions import *
from django.conf import settings

if settings.DEBUG:
    from domofon.settings.local import client_db
else:
    from domofon.settings.prod import client_db


def connect_to_client_DB():
    host = client_db['HOST']
    user = client_db['USER']
    password = client_db['PASSWORD']
    db_name = client_db['NAME']

    conn = pymysql.connect(host=host,
                           user=user,
                           password=password,
                           db=db_name,
                           charset='utf8',
                           cursorclass=pymysql.cursors.DictCursor
                           )
    return conn


def htmlColorToJSON(htmlColor):
    if htmlColor.startswith("#"):
        htmlColor = htmlColor[1:]
    try:
        return {
            "red": int(htmlColor[0:2], 16) / 255.0,
            "green": int(htmlColor[2:4], 16) / 255.0,
            "blue": int(htmlColor[4:6], 16) / 255.0}
    except (TypeError, ValueError):
        raise HTMLcolorError("не правильная Htmlcolor строка")


def is_phone_in_client_db(phonenumb):

    # if settings.CLIENT_DB_E164:
    #     phonenumb = normalize_phone(phonenumb)
    try:
        conn = connect_to_client_DB()
    except ConnectionError:
        return False, 'technical problem with connection to Client service'

    with closing(conn) as conn:
        cur = conn.cursor()
        cur.execute("SELECT uid, mob_phone, dom_phone FROM _client WHERE mob_phone is not null")
        clients = cur.fetchall()

        matched_clients = [client for client in clients if client['mob_phone'] == phonenumb]
        if len(matched_clients) > 1:
            return False, 'more than one clients have this mob_phone number'

        if len(matched_clients) == 1:
            return True, matched_clients[0]

        if not is_mobile_phone_in_ru_kz(phonenumb):
            return False, 'it seems this is not RU or KZ mobile phone number'

    return False, 'no client with this mob_phone number'


def is_mobile_phone_in_ru_kz(number):
    phone = phonenumbers.parse(number, "RU")
    if phone.country_code == 7:
        if number.startswith('+'):
            return len(number[2:]) == 10
        if number.startswith('8'):
            return len(number[1:]) == 10
    return False


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def normalize_phone(number):
    """
    :param phone: raw phone number
    :return: phone number in E164 format
    """
    # E164 = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.E164)
    # return E164
    return number


class SmsService:
    def send_always(self, phone, sms):
        return "success"


def send_sms(phone, sms_text, service=SmsService()):
    """
    send sms by client sms-gate service
    """
    phone = normalize_phone(phone)
    return service.send_always(phone, sms_text)


def get_json(jfile):
    schema = os.path.join(settings.BASE_DIR, 'static', 'jsondata', jfile)

    with open(schema, encoding='utf-8') as j:
        _schema = json.loads(j.read())

    return _schema



# Создать файл фикстуры для таблицы _client
# (БД сервиса клиента, создать тестовых юзеров)


NAMES = ['Ivan', 'Petr', 'Maria', 'Aivaz', 'Revaz', 'Ali', 'Goga', 'Petro', 'Oleg', 'Bonifacy', 'Anton']
SURNAMES = ['Trio', 'Dio', 'Secund', 'Maibah', 'Volvo', 'Gucci', 'Travolta', 'Mendes', 'Buzoff', 'Lopes',
            'Gonsalez']


# random and unique phone numb generator
def random_phone_number(size):
    stack = []
    while True:
        base = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))
        prefix = random.choice('+7', '8')
        phone = prefix + base
        if phone in stack:
            continue
        yield phone


def random_name():
    yield (random.choice(NAMES), random.choice(SURNAMES),)


def create_domofon_user_fixtures(count):
    clients = []
    for i in range(1, count):
        new_client = {
            "uid": i,
            "cid": 0,  # "Компания"
            "blocked": 0,  # "Блокировка"
            "archive": 0,  # "Удален"
            "neclient": 0,  # "Гость"
            "created_at": "0000-00-00 00:00:00",  # "Добавлен"
            "updated_at": "0000-00-00 00:00:00",  # "Обновлен"
            "level": 0,  # "Уровень доступа"

            "did": 0,  # "ID в справочнике"
            "pid": 0,  # "Персонал"
            "lc_aec": None,  # "АЕС"
            "lc_erc": None,  # "ЕРЦ"
            "phone": None,  # "Телефон"
            "name": None,  # "ФИО"
            "info": None,  # "Описание, должность, компания, коментарий"
            "city_meaning": 3,  # "Префикс населённого пункта",
            "city": "Астана",  # "Населённого пункта"
            "street_meaning": None,  # "Префикс улицы"
            "street": None,  # "Улица"
            "street_old": None,  # "Улица (старое)"
            "dom": None,  # "Номер строения"
            "dom_old": None,  # "Номер (старый)"
            "podezd": None,  # "Подъезд"
            "podezd_kv": None,  # "Квартиры подъезда"
            "kv": None,  # "Квартира"
            "zhk": None,  # "Жилой комплекс"

            "category": 0,  # "0 - low, 1-middle, 2-high"
            "birthday": None,  # "День рождения клиента"
            "last_birthday_congrat": None,  # "Штамп времени, когда поздравили крайний раз"
            "happy_birthday": "0",  # "Поздравлять клиента с днем рождения"

            "created_by": None,  # "Автор"

            "updated_by": None,  # "Автор обновления"
            "uon_sync_need": "1",
            "uon_id": "0",
            "mob_phone": random_phone_number(10),  # "Мобильный телефон"
            "dom_phone": None  # "Номер SIP"
        }
        clients.append(new_client)
