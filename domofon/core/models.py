from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.contrib.postgres.fields import JSONField
from os import path
from core.utils import get_json


class DomofonUser(AbstractUser):
    NOT = 'empty'
    MALE = 'male'
    FEMALE = 'female'

    SEX = (
        (NOT, 'Не указан'),
        (MALE, 'Мужской'),
        (FEMALE, 'Женский'),
    )

    phone = models.CharField(verbose_name='Контактный телефон', max_length=30, blank=True)
    phone_confirmed = models.BooleanField(default=False, verbose_name='Телефон подтвержден')
    email_confirmed = models.BooleanField(default=False, verbose_name='Email подтвержден')

    avatar = models.ImageField(max_length=127, verbose_name='Фото профиля',
                               upload_to=path.join('user', 'logo'), default='./user/logo/default.png')

    skype = models.CharField(verbose_name='Логин в Skype', max_length=30, blank=True)

    sex = models.CharField(max_length=6, choices=SEX, default='empty', verbose_name='Пол')
    is_subscriber = models.BooleanField(default=False, verbose_name='Подписан на новости сервиса')

    user_info = JSONField(default=dict, blank=True)

    # связь с учетными записями из client_db
    external_id = models.IntegerField(blank=True, null=True, unique=True)

    def get_available_roles(self):
        if self.is_superuser:
            return Group.objects.all()
        available_groups = []
        for group in self.groups.all():
            if group.name == 'admin':
                groups = Group.objects.all()
                for grp in groups:
                    available_groups.append(grp)
            else:
                available_groups.append(group)
        return set(available_groups)

    def get_roles(self):
        return [g.name for g in self.get_available_roles()]

    @property
    def info(self):
        return self.user_info.get('settings', {})

    @property
    def full_info(self):
        return {
            "sip_id": self.user_info.get('from_client_db', {}).get('dom_phone', None),
            "phone": self.phone,
            "role": self.get_roles()[0],
            "missed_calls": 0,
            "door_phones": [],
            "settings": self.user_info.get('settings', {})
        }

    @staticmethod
    def get_defualt_user_settings():
        jsettings = get_json('user_settings.json')
        default = {}
        for prop, value in jsettings['properties'].items():
            default.update({prop: value['default']})

        return default
